package zahlensortierer;

import java.util.Scanner;
import java.util.Arrays;

/**
 * @author Kolja Blauhut
 *
 */

public class Zahlensortierer {
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		int[] arraysArePrettyCool = new int[3];
		int temp;
		int max;
		int min;
		
		for (int i = 0; i < 3; i++) {
			temp = i + 1;
			System.out.print(temp + ". Zahl: ");
			arraysArePrettyCool[i] = myScanner.nextInt();
		}
		max = Arrays.stream(arraysArePrettyCool).max().getAsInt();
		min = Arrays.stream(arraysArePrettyCool).min().getAsInt();
		
		System.out.println("Max Int: " + max);
		System.out.println("Min Int: " + min);
		myScanner.close();
	}
}